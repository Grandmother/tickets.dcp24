#!/usr/bin/python3.4

import urllib.request
from urllib.error import HTTPError, URLError

from html.parser import HTMLParser

topLevelURL = 'http://noc.dcp24.ru'
statusScreenURL = 'http://noc.dcp24.ru/screen/status'
user = "noc"
password = "Jxca9i3O"

#40300187645 88002500890

def getNocPage():
	authMgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
	authMgr.add_password(None, topLevelURL, user, password)

	handler = urllib.request.HTTPBasicAuthHandler(authMgr)

	opener = urllib.request.build_opener(handler)

	urllib.request.install_opener(opener)

	try:
		response = urllib.request.urlopen(statusScreenURL)
	except HTTPError as e:
		print (e.code)
	return response

class myParser(HTMLParser):
	_pros = list
	def handle_starttag(self, tag, attrs):
		print ("Tag \"", tag, "\" begins.", sep='')
		# if tag == "tr":
			# if attrs[0][0] == "class" and attrs[0][1] == "header":
			# 	print ("")
		if tag == "div":
			print ("Div attributes: \n\t", attrs)
			print ("Div class: ", attrs[0][0])
	def handle_endtag(self, tag):
		print ("Tag \"", tag, "\" ends.", sep='')
	def handle_data(self, data):
		if all(field != '\n' for field in data):
			print ("\tTags data: ", data, sep='')

page = getNocPage()

nocParser = myParser()

# tmpPage = page
# pageText = tmpPage.read().decode('utf-8')
# for i in pageText:
# 	if i.find("header middle"):
# 		print(i)
# 	if i.find("header"):
# 		conLost = True
# 		print(i)

nocParser.feed(page.read().decode('utf-8'))